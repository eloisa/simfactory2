#! /bin/bash

echo "Preparing:"
set -x                          # Output commands
set -e                          # Abort on errors

cd @RUNDIR@-active

echo "Checking:"
pwd
hostname
date

echo "Environment:"
export CACTUS_NUM_PROCS=@NUM_PROCS@
export CACTUS_NUM_THREADS=@NUM_THREADS@
export GMON_OUT_PREFIX=gmon.out
export OMP_NUM_THREADS=@NUM_THREADS@
env | sort > SIMFACTORY/ENVIRONMENT
if [ "x$HOSTFILE" != x ] ; then
  for node in $(sort -u $HOSTFILE); do
    for ((proc=0; $proc<@(@PPN_USED@ / @NUM_THREADS@)@; proc=$proc+1)); do
        echo ${node}
    done
  done > MPI_HOSTFILE
fi

echo "Starting:"
export CACTUS_STARTTIME=$(date +%s)
@SOURCEDIR@/exe/@CONFIGURATION@/mpirun $(env | awk -vFS== '$1!="_"{print "-x",$1}') -np @NUM_PROCS@ ${HOSTFILE+-machinefile MPI_HOSTFILE} @EXECUTABLE@ -L 3 @PARFILE@

echo "Stopping:"
date

echo "Done."
